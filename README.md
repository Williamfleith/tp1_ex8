**Readme Ex8:**

Created on 24/04/2020 by William FLEITH


**Objective :** In this exercice we wanted to make a file setup.py to enable the installation of our package, with it we don't need to set properly the PYTHONPATH any more. SO it is very powerfull because with this we can install the package we need and uninstall it when we don't need them any more.


**Pylint :** The programm ex8.py is rated 10/10. But the test.py is only rated 9.5/10.


**Running with setup.py:**
- go in the repository tp1_ex8
- enter the following commands : 
	- python setup.py sdist
	- pip install dist/TestSimpleCalculator-0.0.1.tar.gz
	- pip freeze | grep Test (with this u can check if the package has been well installed)
go to the folder test and enter the command : python test.py 





